##Getting started:

Подготавливаем пакеты
```
apt install nodejs git
```
Стабильная версия nodejs
```
npm install -g n
n stable
```
Скачиваем проект из репозитория
```
mkdir steamCmd
cd steamCmd
git init
git remote add origin https://k7eon@bitbucket.org/k7eon/steamcmd.git
git pull origin master
```
Обновляем зависсимости проекта
```
npm update
```
Копируем пример конфига. Нужно заполнить своими данными
```
cp config.example.json config.json
```
### Заметки
Запуск скрипта
```
node start
или
node start.js
```
Установка/удаление зависимости из проекта и package.json
```
npm install ws --save
npm remove ws --save
```
Установка/удаление зависимости из проекта, но не затрагивая package.json
```
npm install ws
npm remove ws
```
Установка/удаление глобальной зависимости из проекта
```
npm install ws -g --save
npm remove ws -g --save
```

### Команды
- 1 - вывод кода авторизации для аккаунта (код используется для авторизации в браузере)
- 2 - получает sessionid{string} и cookies{string}, которые используются для авторизованных GET/POST запросов к steam
- 3 - подтверждает **все** операции, требующие мобильного подтверждения (выставление на продажу, подтверждение обмена)

