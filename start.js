const
    SteamCommunity = require('steamcommunity'),
    Totp = require('steam-totp'),
    colors = require('colors'),
    _ = require('lodash'),
    fs = require('fs'),
    readline = require('readline');

let exit = () => {process.exit(1)}

let community = new SteamCommunity()
let options = JSON.parse(fs.readFileSync('./config.json', 'utf8'))

let mobile_confirmation = (callback) => {
    console.log('mobile_confirmation...')
    let time = Math.floor(Date.now() / 1000) + this.offset

    community.acceptAllConfirmations(
        time,
        Totp.getConfirmationKey(options.identity_secret, time, "conf"),
        Totp.getConfirmationKey(options.identity_secret, time, "allow"),

        (err, confirmations) => {
            if (err) {
                callback(err, false)

                if (err == 'Error: Could not act on confirmation') {
                    console.log('community.acceptAllConfirmations err'.red, err)
                    return
                }

                console.log(`ERROR mobile: ${err}`.red)
                return
            }


            if (!confirmations) {
                callback(null, true)
                return
            }

            confirmations.forEach((item) => {
                console.log(`confirm: ${item.id}`)
            })
            callback(null, true)
        }
    )

}

/**
 * Принимает обмен
 * @param trade_offer_id
 * @param callback
 */
let accept = (trade_offer_id, sessionId, callback) => {
    console.log('\naccepting'.magenta);
    // console.log('trade_offer_id', trade_offer_id);
    // console.log('sessionId', sessionId);

    let accept_config = {
        url: `https://steamcommunity.com/tradeoffer/${trade_offer_id}/accept`,
        headers: {
            Origin:'https://steamcommunity.com',
            Referer:`https://steamcommunity.com/tradeoffer/${trade_offer_id}`,
            'User-Agent':'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.125 YaBrowser/17.7.0.1685 Yowser/2.5 Safari/537.36'
        },
        form: {
            sessionid: sessionId,
            serverid: 1,
            tradeofferid: trade_offer_id,
            // partner: partner,
            captcha: ''
        },
        json: true,
    };

    community.httpRequestPost(accept_config, (err, response, body) => {
        if (err) {
            callback(false, null);
            if (err.message === 'Could not act on confirmation') {
                console.log('Could not act on confirmation');
                return
            }

            console.log('Account.accept error: ');
            console.log(err.message);
            return
        }

        console.log(`/${trade_offer_id}/accept`, body);
        if (body.needs_mobile_confirmation) {
            console.log('Trade accepted');
            callback(true, true);
            return;
        }
        callback(true, null);
    })
};

let awailable_commands = [
    '--help',
    '-totp', '1',
    '-cookie', '2',
    '-mconf', '3',
    '-accepttrades', '4',
    '5',
];

// Не найдено валидной команды
if (!_.some(awailable_commands, (t) => {
        return process.argv.indexOf(t) > -1
    })) {
    console.log('Incorrect command. Use '.red+'node start --help'.blue+' for list of available commands.'.red)
    exit()
    return
}

if (process.argv.includes('--help')) {
    console.log(`
${'--help'.blue}        - Help information
--------
${'-totp'.blue}   or ${'1'.blue}  - generate totp auth code
${'-cookie'.blue} or ${'2'.blue}  - authorize and receive cookies and sessionid
${'-mconf'.blue}  or ${'3'.blue}  - authorize and confirm all 'confirmations' by phone app 
${'-accepttrades'.blue}  or ${'4'.blue}  - authorize and accept with confirm all inputted urls 
${'5'.blue}  - authorize and accept with confirm all inputted offer_ids 
--------
Example of usage:
${'node start 1'.blue}
or
${'node start -totp'.blue}
    `)
    exit()
    return
}

Totp.getTimeOffset((err, offset, latency) => {
    if (err) {
        console.log('Totp.getTimeOffset error: '.red, err)
        exit()
        return
    }

    this.offset = offset
    console.log(`offset: ${this.offset}`.gray)

    // totp
    if (process.argv.includes('-totp') || process.argv.includes('1')) {
        console.log('twoFactorCode'.blue, Totp.generateAuthCode(options.shared_secret, offset))
        exit()
        return
    }

    let community_config = {
        "accountName": options.username,
        "password": options.password,
        "twoFactorCode": Totp.generateAuthCode(options.shared_secret, offset)
    };

    community.login(community_config, (err, sessionId, cookies, steamGuard, oAuthToken) => {
        if (err) {
            console.log('Steam authorize error: '.red, err.message)
            exit()
            return
        }

        if (process.argv.includes('-cookie') || process.argv.includes('2')) {
            console.log('sessionId'.blue, sessionId)
            console.log('cookies'.blue, cookies.join('; '))
            exit()
            return
        }

        if (process.argv.includes('-mconf') || process.argv.includes('3')) {
            mobile_confirmation((err, success) => {
                if (err) {
                    console.log(err)
                    exit()
                    return
                }

                exit()
            })
            return
        }

        if (process.argv.includes('-accepttrades') || process.argv.includes('4')) {
            console.log('Введите url обмена');
            process.stdin.resume();
            process.stdin.setEncoding('utf8');
            process.stdin.on('data', (chunk) => {
                chunk = chunk.split('/');
                let offerId = chunk[chunk.length-1].trim();
                process.stdout.write('offerId: ' + offerId);

                accept(offerId, sessionId, (success) => {
                    if (!success) {
                        console.log('Не удалось принять обмен');
                        return;
                    }

                    mobile_confirmation((err, success) => {
                        if (err) {
                            console.log(err);
                            return;
                        }
                    })
                })
            });
        }

        if (process.argv.includes('5')) {
            const urlapi = require('url');

            console.log('Введите offer_id обмена:');
            process.stdin.resume();
            process.stdin.setEncoding('utf8');
            process.stdin.on('data', function (chunk) {

                let offerId = '';
                if (chunk.indexOf('/') !== -1) {
                    //https://steamcommunity.com/tradeoffer/3014607172
                    offerId = urlapi.parse(chunk).path.split('/').filter(n => n)[1];
                } else {
                    offerId = chunk.trim();
                }

                process.stdout.write('offerId: ' + offerId);

                accept(offerId, sessionId, (success, need_mobile_confirmation) => {
                    if (!success) {
                        console.log('Не удалось принять обмен');
                        return;
                    }

                    if (need_mobile_confirmation) {
                        mobile_confirmation((err, success) => {
                            if (err) {
                                console.log(err);
                                return;
                            }
                        })
                    }
                })
            });
        }
    })
})